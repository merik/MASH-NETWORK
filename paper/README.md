# ICASSP 2019
All submission rules deadlines etc. can be found [here](https://2019.ieeeicassp.org/paperkit#FAQ1)
## IEEE template package
The IEEEtran manual can be found [here](./template/IEEEtran_HOWTO.pdf)
