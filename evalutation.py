import numpy as np
import h5py
import AnalogToDigital as ADC
import AnalogToDigital.deltaSigma as deltaSigma
from scipy import signal

def calculateSNR(psd, OSR, offset=10):
    n = len(psd)
    # Clear out lowpass offset
    psd = np.abs(psd[2:int(n/(OSR))])
    # Find peak (Sinusoid)
    tmp = np.argmax(psd)
    # Range for signal spectrum
    signalIndex = np.arange(int(tmp - offset/2), int(tmp + offset/2), dtype=np.int)
    # Compute signal power
    signalPower = np.sum(psd[signalIndex])
    # Compute noise range index
    noiseIndex = np.delete(np.arange(psd.size), signalIndex)
    # Compute noise power
    noisePower = np.sum(psd[noiseIndex])
    # Calculate snr linear
    snr = signalPower/noisePower
    return snr, signalPower, noisePower


def simualteSin(amplitude, frequency, phase, osr, order_mash_block, n_lev, size):
    """
    This runs the Sigma Delta Comparision
    """
    size = int(size  * 2)


    """
    Input signal
    """
    dummy = np.zeros(np.sum(order_mash_block))
    input = ADC.system.Sin(1., amplitude, frequency, phase, steeringVector=dummy)

    """
    Simulation Variables
    """
    t = np.linspace(0., size - 1 , size)
    u = input.scalarFunction(t)


    """
    Setup MASH
    """
    ds = [deltaSigma.DeltaSigma(osr, x, nlev = n_lev) for x in order_mash_block]
    mash = deltaSigma.MASH(osr, ds)


    """
    Generate Data
    """
    bits = mash.simulate(input, t)[int(size/2):,:]

    u_hat = mash.reconstruction(bits)

    return u, bits, u_hat

# Simulates an sinusodial MASH input

def getSinusData(amplitude, osr, order_mash_block, n_lev, size):
    # two thirds of fbandwidth
    frequency = 1./(2 * osr) * 2. / 3.
    phase = 0.
    u, bits, u_hat = simualteSin(amplitude, frequency, phase, osr, order_mash_block, n_lev, size)
    return bits, u_hat


# Computes the power spectral density using Welch method
def spectrum(s):
    f, psd = signal.welch(s, 1, nperseg=1024 * 32)
    return psd
