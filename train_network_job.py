import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data
from torch.autograd import Variable
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import glob
import os
import _pickle as pickle
import logging
from logging import FileHandler
from logging import Formatter
import sys
import argparse

# import externally defined network structures and data loaders
import auxiliary as aux


# messaging logger
def create_logger(name, fname):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger_file_handler = FileHandler(fname)
    logger_file_handler.setLevel(logging.INFO)
    logger_file_handler.setFormatter(Formatter("%(asctime)s: %(message)s"))
    logger.addHandler(logger_file_handler)
    return logger

log_general = create_logger("general", "./Logfiles/_general.log")
pid  = os.getpid()

# Instantiate the parser
parser = argparse.ArgumentParser()
# Required positional argument
parser.add_argument('conf_name', type=str, help='Configuration name (required)')
parser.add_argument('-r', action='store_true', help='Forced restart. Ignores all checkpoints and starts from scratch.')
parser.add_argument('-c', type=int, help='Checkpoint to start from.')
parser.add_argument('-e', type=int, help='Number of epochs.')
# parser.add_argument('-b', type=int, help='Number of runs per batch.')
parser.add_argument('-s', type=int, help='Take snapshot (save network state, plot performance) every .. epoch.')

args = parser.parse_args()


# grab configuration name passed as argument
conf_name = args.conf_name
# create configuration specific log file
log_config = create_logger("config", './Logfiles/%s.log' % conf_name)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
log_config.info('----------------------------------------------------------------------------')
log_config.info('call: ' + ' '.join(sys.argv))
log_config.info('PID: %i, device: %s' % (pid, device))


# trying to pick up the most recent checkpoint to continue with training
try:
    c = aux.conf[conf_name]
except:
    log_general.info('config "%s" not available!' % conf_name)
    exit()

# load checkpoint if specified
ckp_fname = ''
if args.c:
    ckp_fname = conf_name + ('.ckp#%i' % args.c)
else:
    try:
        ckp_fname = sorted(glob.glob('./Networks/%s.ckp*' % conf_name), key=os.path.getmtime)[-1].split('/')[2]
    except:
        pass

if args.r: # if restart was specified, start from scratch
    ckp_fname = ''

# define empty network, optimizer and loss function
net = c['network']().double().to(device)
optimizer = optim.SGD(net.parameters(), lr=c['learning_rate'], momentum=c['momentum'])
criterion = nn.MSELoss()
epoch_offset = 0

# if checkpoint available, load state
if ckp_fname:
    try:
        state = torch.load('./Networks/' + ckp_fname)
        net.load_state_dict(state['network_dict'])
        optimizer.load_state_dict(state['optimizer_dict'])
        epoch_offset = state['epoch']
        log_config.info('checkpoint "%s" loaded' % ckp_fname)
    except Exception as e:
        log_config.info('loading of checkpoint failed! (%s)' % e)
else:
    log_config.info('network created')


# loading training data set
log_config.info("loading dataset from file ..")
ds_load_train = pickle.load(open('./Datasets/' + c['dataset_train'], 'rb', -1))
params_train = {'batch_size': c['batch_size'], 'shuffle': True, 'num_workers': 4}
ds_train = aux.DatasetStream(ds_load_train['y'], ds_load_train['x'], c)
data_loader_train = data.DataLoader(ds_train, **params_train) # a data loader is a data set and a sampler
log_config.info("loading done. length of training data set: %i samples" % ds_train.__len__())

# loading test data set
ds_load_eval = pickle.load(open('./Datasets/' + c['dataset_test'], 'rb', -1))
params_eval = {'batch_size': 100, 'shuffle': False, 'num_workers': 4}
ds_eval = aux.DatasetStream(ds_load_eval['y'], ds_load_eval['x'], c)
data_loader_eval = data.DataLoader(ds_eval, **params_eval)

log_config.info('starting training from epoch %i' % (epoch_offset + 1))

# eval procedure
def eval_ds():
    n_samples = 0
    mse_sum = 0.0
    for i, (batch_data, batch_labels) in enumerate(data_loader_eval):
        batch_data = Variable(batch_data.double()).cuda(async=True)
        batch_labels = Variable(batch_labels.double()).cuda(async=True)
        n_batch_now = batch_labels.shape[0]
        y_hat = net(batch_data).cpu().detach().numpy().flatten()

        mse_sum += ((y_hat - batch_labels.cpu()) ** 2).mean() * len(batch_labels)
        n_samples += len(batch_labels)
        del batch_data, batch_labels, y_hat
    return mse_sum / n_samples



# training of model
n_epochs = 100000 # how many additional epochs?
n_iter_per_batch = c['n_iter_batch'] # numbers of iteration per batch
n_snapshot = 1

# overwrite with possible arguments
if args.e:
    n_epochs = args.e
if args.s:
    n_snapshot = args.s

mses_train = np.zeros(n_epochs)
mses_test = np.zeros(n_epochs)
mses_train[0] = 0
mses_test[0] = 0

fig, ax = plt.subplots(1,1)
fig.set_size_inches(14, 7)
ax.plot(mses_train, label='train')
ax.plot(mses_test, label='test')
ax.set_ylabel('10*log(MSE) (dB)')
ax.set_xlabel('epoch')
plt.legend()

for epoch in range(n_epochs):
    epoch_total = epoch_offset + epoch + 1

    mse_train_sum = 0.0
    n_of_mses = 0

    for i, (batch_data, batch_labels) in enumerate(data_loader_train):
        batch_data = Variable(batch_data.double()).cuda(async=True)   # maybe add double here
        batch_labels = Variable(batch_labels.double()).cuda(async=True)

        # train each bach multiple times
        for i_batch in range(n_iter_per_batch):
            # train network
            optimizer.zero_grad()
            outputs = net(batch_data).view(-1)
            loss = criterion(outputs, batch_labels)
            loss.backward()
            optimizer.step()
            mse_train_sum += loss.data.item()
            n_of_mses += 1
        del batch_data, batch_labels, outputs

    # validate with test data set
    mse_test = eval_ds()
    mse_train = mse_train_sum / n_of_mses
    mses_train[epoch] = mse_train
    mses_test[epoch] = mse_test

    log_config.info('PID: %i, epoch: %i, MSE_train: %.10f (%.5f dB), MSE_test: %.10f (%.5f dB),'
                % (pid, epoch_total, mse_train, 10*np.log10(mse_train), mse_test, 10*np.log10(mse_test)))

    # plot live loss function
    ax.lines[0].set_ydata(10*np.log10(mses_train[0:epoch + 1]))
    ax.lines[0].set_xdata( range(epoch_offset + 1, epoch_offset + epoch + 2))
    ax.lines[1].set_ydata(10*np.log10(mses_test[0:epoch + 1]))
    ax.lines[1].set_xdata( range(epoch_offset + 1, epoch_offset + epoch + 2))
    ax.relim()
    ax.autoscale_view()
    fig.canvas.draw()

    # take snapshot
    if (epoch + 1) % n_snapshot == 0:
        # save plot
        fig.savefig('./Plots/%s.png' % conf_name, bbox_inches='tight')
        # saving network
        state = {'epoch': epoch_total, 'network_dict': net.state_dict(), 'optimizer_dict': optimizer.state_dict() }
        torch.save(state, './Networks/%s.ckp#%i' % (conf_name, epoch_total))
