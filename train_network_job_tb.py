import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data
from torch.autograd import Variable
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import glob
import os
import _pickle as pickle
import math
import logging
from logging import FileHandler
from logging import Formatter
import sys
import argparse
from time import gmtime, strftime
from tensorboardX import SummaryWriter
import h5py
import evalutation
import multiprocessing as mp
from datetime import datetime

# import externally defined network structures and data loaders
import auxiliary as aux
import configuration as conf
filename = 'dataStore.hdf5'


# messaging logger
def create_logger(name, fname):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger_file_handler = FileHandler(fname)
    logger_file_handler.setLevel(logging.INFO)
    logger_file_handler.setFormatter(Formatter("%(asctime)s: %(message)s"))
    logger.addHandler(logger_file_handler)
    return logger


log_general = create_logger("general", "./Logfiles/_general.log")
pid = os.getpid()

# Instantiate the parser
parser = argparse.ArgumentParser()
# Required positional argument
parser.add_argument('conf_name', type=str,
                    help='Configuration name (required)')
parser.add_argument('-r', action='store_true',
                    help='Forced restart. Ignores all checkpoints and starts from scratch.')
parser.add_argument('-c', type=int, help='Checkpoint to start from.')
parser.add_argument('-e', type=int, help='Number of epochs.')
# parser.add_argument('-b', type=int, help='Number of runs per batch.')
parser.add_argument(
    '-s', type=int, help='Take snapshot (save network state, plot performance) every .. epoch.')

args = parser.parse_args()


# grab configuration name passed as argument
conf_name = args.conf_name
# create configuration specific log file
log_config = create_logger("config", './Logfiles/%s.log' % conf_name)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
log_config.info(
    '----------------------------------------------------------------------------')
log_config.info('call: ' + ' '.join(sys.argv))
log_config.info('PID: %i, device: %s' % (pid, device))


# tensorboard
t_str = strftime("%Y_%m_%d__%H_%M_%S", gmtime())
writer = SummaryWriter('runs/{}/{}'.format(conf_name, t_str))


# trying to pick up the most recent checkpoint to continue with training
try:
    c = conf.load()[conf_name]
except:
    log_general.info('config "%s" not available!' % conf_name)
    exit()

# load checkpoint if specified
ckp_fname = ''
if args.c:
    ckp_fname = conf_name + ('.ckp#%i' % args.c)
else:
    try:
        ckp_fname = sorted(glob.glob('./Networks/%s/%s.ckp*' %
                                     (conf_name, conf_name)), key=os.path.getmtime)[-1].split('/')[-1]
    except:
        pass

if args.r:  # if restart was specified, start from scratch
    ckp_fname = ''

# define empty network, optimizer and loss function
net = eval('aux.' + c['network'])(c['network_params']).double().to(device)

# Choose optimiser according to settings in configuration file
if 'optim' in c:
    parameters_optim = c['optim']
    log_config.info("Setting up optimiser for:\n%s" % parameters_optim)
    if parameters_optim['name'] == 'SGD':
        optimizer = optim.SGD(net.parameters(), lr=parameters_optim['lr'], momentum=parameters_optim['momentum'])
    elif parameters_optim['name'] == 'Adadelta':
        optimizer = optim.Adadelta(net.parameters(), lr=parameters_optim['lr'], rho=parameters_optim['rho'], eps=parameters_optim['eps'], weight_decay=parameters_optim['weight_decay'])
    elif parameters_optim['name'] == 'Adagrad':
        optimizer = optim.Adagrad(net.parameters(), lr=parameters_optim['lr'], lr_decay=parameters_optim['lr_decay'], weight_decay=parameters_optim['weight_decay'], initial_accumulator_value=parameters_optim['initial_accumulator_value'])
    elif parameters_optim['name'] == 'Adam':
        optimizer = optim.Adam(net.parameters(), lr=parameters_optim['lr'], betas=parameters_optim['betas'], eps=parameters_optim['eps'], weight_decay=parameters_optim['weight_decay'], amsgrad=parameters_optim['amsgrad'])
    elif parameters_optim['name'] == 'Adamax':
        optimizer = optim.Adamax(net.parameters(), lr=parameters_optim['lr'], betas=parameters_optim['betas'], eps=parameters_optim['eps'], weight_decay=parameters_optim['weight_decay'])
    elif parameters_optim['name'] == 'ASGD':
        optimizer = optim.ASGD(net.parameters(), lr=parameters_optim['lr'],  lambd=parameters_optim['lambda'], alpha=parameters_optim['alpha'], t0=parameters_optim['t0'], weight_decay=parameters_optim['weight_decay'])
    elif parameters_optim['name'] == 'RMSprop':
        optimizer = optim.RMSprop(net.parameters(), lr=parameters_optim['lr'],   alpha=parameters_optim['alpha'], eps=parameters_optim['eps'], weight_decay=parameters_optim['weight_decay'], momentum=parameters_optim['momentum'], centered=parameters_optim['centered'])
    elif parameters_optim['name'] == 'Rprop':
        optimizer = optim.Rprop(net.parameters(), lr=parameters_optim['lr'], etas=parameters_optim['etas'], step_sizes=parameters_optim['step_sizes'])
    else:
        log_config.error("No optmiser with that name.")
        raise ValueError(parameters_optim)
else:
    optimizer = optim.SGD(net.parameters(), lr=c['learning_rate'], momentum=c['momentum'])





criterion = nn.MSELoss()
epoch_offset = 0

# if checkpoint available, load state
if ckp_fname:
    try:
        print(ckp_fname)
        state = torch.load('./Networks/%s/%s' % (conf_name, ckp_fname))
        net.load_state_dict(state['network_dict'])
        optimizer.load_state_dict(state['optimizer_dict'])
        epoch_offset = state['epoch']
        log_config.info('checkpoint "%s" loaded' % ckp_fname)
    except Exception as e:
        log_config.info('loading of checkpoint failed! (%s)' % e)
else:
    log_config.info('network created')


# Choose learning rate scheduler
if 'learning_rate_scheduler' in c:
    lr_schedule = True
    lr_param = c['learning_rate_scheduler']
    # lr_param['last_epoch'] = epoch_offset
    log_config.info("Setting up learning rate scheduler:\n%s" % lr_param)
    if lr_param['name'] == 'StepLR':
        lr_scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=lr_param['step_size'], gamma=lr_param['gamma'], last_epoch=lr_param['last_epoch'])
        for i in range(epoch_offset):
            lr_scheduler.step()
    elif lr_param['name'] == 'none':
        lr_schedule = False
    else:
        log_config.error("No learning rate scheduler with that name.")
        raise ValueError(lr_param)
else:
    lr_schedule = False


# write tensorboard stuff
writer.add_text('Text', 'pid: %i, device: %s' %
                (pid, device), epoch_offset + 1)
writer.add_text('Text', 'call: %s' % ' '.join(sys.argv), epoch_offset + 1)
writer.add_text('Text', 'config: %s' % str(c), epoch_offset + 1)

# preparing datasets
params_train = {'batch_size': c['batch_size'], 'shuffle': True, 'num_workers': mp.cpu_count()}
params_eval = {'batch_size': 1000, 'shuffle': False, 'num_workers': mp.cpu_count()}
params_spec = {'batch_size': 1000, 'shuffle': False, 'num_workers': mp.cpu_count()}

# Simulate sinus at almost 1 / ( 2 osr )
mash_orders = [int(x) for x in c['device'][4:].split('_') if x != '']
sizeOfSpecSimulation = 100000
sinDataSampleX, sinDataSampleY = evalutation.getSinusData(amplitude = 1./np.sqrt(2.), osr = c['OSR'],
                                                        order_mash_block=mash_orders, n_lev = 2, size=sizeOfSpecSimulation)
ds_spec = aux.DatasetStore({'x':sinDataSampleX, 'y':sinDataSampleY}, c)
data_loader_spec = data.DataLoader(ds_spec, **params_spec)

snrSpec = evalutation.calculateSNR(evalutation.spectrum(sinDataSampleY), c['OSR'], offset=10)
log_config.info('expected SNR: %.2f dB' % (10 * np.log10(snrSpec[0])))

# log_config.info("loading done. length of training data set: %i samples, length of eval data set: %i" % (
#     ds_train.__len__(), ds_eval.__len__()))
log_config.info('starting training from epoch %i' % (epoch_offset + 1))



# eval procedure
def eval_ds():
    # iterate randomly over all datasets
    idx_ds = np.arange(len(c['dataset_test']))
    np.random.shuffle(idx_ds)

    n_samples = 0
    mse_sum = 0.0
    for ii in idx_ds:
        ds_key = c['dataset_test'][ii] # contains hdf5 key of dataset
        ds_eval = aux.DatasetStream(filename, ds_key, c)
        data_loader_eval = data.DataLoader(ds_eval, **params_eval)
        # iterate over current dataset
        for i, (batch_data, batch_labels) in enumerate(data_loader_eval):
            batch_data = Variable(  batch_data.double().cuda() )
            batch_labels = Variable( batch_labels.double().cuda() )
            y_hat = net(batch_data).cpu().detach().numpy().flatten() # feed data through network
            mse_sum += ((y_hat - batch_labels.cpu()) ** 2).mean() * len(batch_labels) # this is the summed squared error
            n_samples += len(batch_labels)
    return mse_sum / n_samples


# Function to evaluate output for a whole sweeping window
def spec_ds(neuralNetwork):
    y = np.zeros(ds_spec.__len__())
    n_batch = params_spec['batch_size']
    for i, (batch_data, batch_labels) in enumerate(data_loader_spec):
        batch_data = Variable(batch_data.double().cuda())
        y_b = neuralNetwork(batch_data).cpu().detach().numpy().flatten()
        n_batch_now = len(y_b)
        y[i * n_batch: i * n_batch + n_batch_now] = y_b
    return y


# training of model
n_epochs = 100000000  # how many additional epochs?
n_iter_per_batch = c['n_iter_batch']  # numbers of iteration per batch
n_snapshot = 1

# overwrite with possible arguments
if args.e:
    n_epochs = args.e
if args.s:
    n_snapshot = args.s


# calculate some numbers for tensorboard logging purposes
n_of_batches = math.ceil(100000 / c['batch_size'])      ####################### hardcoded value, change that
n_of_steps_per_epoch = n_of_batches * n_iter_per_batch * len(c['dataset_train'])
log_config.info('n_of_batches: %i, n_of_steps_per_epoch: %i, n_of_training_sets %i' % (n_of_batches, n_of_steps_per_epoch, len(c['dataset_train'])))

# start timer
t_chckp = datetime.now()
t_eval = datetime.now()

try:
    for epoch in range(n_epochs):
        epoch_total = epoch_offset + epoch + 1
        n_steps_offset = (epoch_total - 1) * n_of_steps_per_epoch
        step_cnt = 0 # counts every optimization step

        # Apply adaptive learning rate
        if lr_schedule:
            # Decrease learning rate
            lr_scheduler.step()

        # iterate randomly over all datasets
        idx_ds = np.arange(len(c['dataset_train']))
        np.random.shuffle(idx_ds)

        # iterate over all available datasets and treat them separately
        for ii in idx_ds:
            ds_key = c['dataset_train'][ii]  # contains hdf5 key of dataset
            ds_train = aux.DatasetStream(filename, ds_key, c)
            data_loader_train = data.DataLoader(ds_train, **params_train)

            # iterate over all available batches
            for i_batch, (batch_data, batch_labels) in enumerate(data_loader_train):
                # move data to cuda
                batch_data = Variable(batch_data.double().cuda())
                batch_labels = Variable(batch_labels.double().cuda())

                if len(batch_labels > 20): # prevent overfitting
                    # train each batch multiple times
                    for i_batch_iter in range(n_iter_per_batch):
                        step_cnt += 1
                        # train network
                        optimizer.zero_grad()
                        outputs = net(batch_data).view(-1)
                        loss = criterion(outputs, batch_labels)
                        loss.backward()
                        optimizer.step()
                        mse_train_step = loss.data.item()

                        # log MSE for every optimization step
                        if i_batch_iter in [0, n_iter_per_batch-1]:
                            writer.add_scalar('data/MSE_train', 10 * np.log10(mse_train_step), n_steps_offset + step_cnt - 1)


            # after every processed dataset, check if time has ran out -> if yes, save checkpoint and evaluate network
            # take snapshot every hour
            t_now = datetime.now()
            if (t_now - t_chckp).total_seconds() > 60*60:
                t_chckp = t_now
                log_config.info('save state %i' % epoch_total)
                dir = os.path.dirname('./Networks/%s/' % conf_name)
                if not os.path.exists(dir):
                    os.makedirs(dir)
                # saving network
                state = {'epoch': epoch_total, 'network_dict': net.state_dict(
                ), 'optimizer_dict': optimizer.state_dict()}
                torch.save(state, './Networks/%s/%s.ckp#%i' %
                           (conf_name, conf_name, epoch_total))


            # validate with test data set every xx minutes
            t_now = datetime.now()
            if (t_now - t_eval).total_seconds() > 15*60:
                t_eval = t_now
                # log_config.info('start eval')
                mse_test = eval_ds()
                # log_config.info('calc snr')
                snrSpec = evalutation.calculateSNR(evalutation.spectrum(spec_ds(net)), c['OSR'], offset=10)
                # print(snrSpec)
                log_config.info('PID: %i, epoch: %i, MSE_train_step (last): %.2f dB, MSE_test: %.2f dB, SNR_SPEC: %.2f dB'
                                % (pid, epoch_total, 10 * np.log10(mse_train_step), 10 * np.log10(mse_test), 10 * np.log10(snrSpec[0])))

                # write tensorboard logfiles
                writer.add_scalar('data/MSE_eval', 10 * np.log10(mse_test), n_steps_offset + step_cnt - 1)
                writer.add_scalar('data/SNR_Spec', 10 * np.log10(snrSpec[0]), n_steps_offset + step_cnt - 1)

except Exception as e:
    log_config.info('Learning process failed! (%s)' % e)
