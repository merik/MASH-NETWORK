import numpy as np

import matplotlib.pyplot as plt
plt.switch_backend('agg')


def dbToDecimal(db):
    return np.power(10, (db / 10.0))

def decimalToDb(decimal):
    return 10.0 * np.log10(decimal)

dec = 0.0000000000000000000000000000000001234
# dbs = -100.1
# dec = dbToDecimal(dbs)
dbs_ = decimalToDb(dec)
dec_ = dbToDecimal(dbs_)

# print('{}dB is {} and converted back {}'.format(dbs, dec, dbs_))
print('{0:.64f} is \n{1:.64f}dB and converted back \n{2:.64f}'.format(dec, dbs_, dec_))
