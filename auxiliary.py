# this file defines auxiliary methods / classes:
# - Dataset
# - network structures

import numpy as np
import h5py
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data
from torch.autograd import Variable


# Dataset class if bit data comes as stream. __getitem__ is selecting the window of bits of the right size.
class DatasetStream(data.Dataset):
    def __init__(self, filename, dataset, conf):
        """
        filename is the name of the h5py dataStore
        dataset is a key / dataset in the h5py datastore
        conf is the configuration file
        """
        self.filename = filename
        self.window_size = conf['window_size']
        self.sample_offset = conf['sample_offset']
        self.configuration = conf

        # load dataset from hdf5 container
        with h5py.File(self.filename, 'r') as f:
            self.x = torch.from_numpy(np.array( f[dataset][conf['labels']['x']], dtype=np.float64))
            self.y = torch.from_numpy(np.array( f[dataset][conf['labels']['y']], dtype=np.float64))

        self.length = self.y.shape[0] - self.window_size # its actually one sample longer, but shortened such that batches are similar in size

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        return self.x[index : index+self.window_size].view(-1), self.y[index + self.sample_offset - 1]




# needs to be changed as well
class DatasetStore(data.Dataset):
    """
    This dataset store converts a numpy data set {x:, y:}
    into an torch dataloader
    """
    def __init__(self, dataset, conf):
        self.dataset = dataset
        self.window_size = conf['window_size']
        self.sample_offset = conf['sample_offset']
        self.length = dataset['y'].shape[0]
        self.length -= self.window_size

    def __len__(self):
        return self.length

    def loader(self, index):
        """
        loads the dataset from the h5py store and converts it to a pytorch tensor
        """
        x = torch.from_numpy(np.array(self.dataset['x'][index: index + self.window_size], dtype=np.float64)).view(-1)
        y = torch.from_numpy(np.array(self.dataset['y'][self.sample_offset + index]))
        return x, y

    def __getitem__(self, index):
        return self.loader(index)


class NetTest(nn.Module):
    def __init__(self):
        super(NetTest, self).__init__()

        self.lin_1=nn.Linear(300, 300)
        self.lin_2=nn.Linear(300, 100)
        self.lin_3=nn.Linear(100, 1)
        self.rl_1=nn.ReLU()
        self.rl_2=nn.ReLU()
        # self.rl_3 = nn.ReLU()

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.lin_2(x)
        x=self.rl_2(x)
        x=self.lin_3(x)
        # x = self.rl_3(x)
        return x


class NetTestLarge(nn.Module):
    def __init__(self):
        super(NetTestLarge, self).__init__()

        self.lin_1=nn.Linear(600, 600)
        self.lin_2=nn.Linear(600, 100)
        self.lin_3=nn.Linear(100, 1)
        self.rl_1=nn.ReLU()
        self.rl_2=nn.ReLU()
        # self.rl_3 = nn.ReLU()

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.lin_2(x)
        x=self.rl_2(x)
        x=self.lin_3(x)
        # x = self.rl_3(x)
        return x


class NetTestDropout(nn.Module):
    def __init__(self):
        super(NetTestDropout, self).__init__()

        self.lin_1=nn.Linear(300, 300)
        self.do=nn.Dropout(0.3)
        self.lin_2=nn.Linear(300, 100)
        self.lin_3=nn.Linear(100, 1)
        self.rl_1=nn.ReLU()
        self.rl_2=nn.ReLU()
        # self.rl_3 = nn.ReLU()

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.do(x)
        x=self.lin_2(x)
        x=self.rl_2(x)
        x=self.lin_3(x)
        # x = self.rl_3(x)
        return x


class NetLinearLayer(nn.Module):
    def __init__(self, network_params = {'n_taps': 1, 'n_inputs': 100}):
        super(NetLinearLayer, self).__init__()
        self.lin_1 = nn.Linear(network_params['n_inputs'], network_params['n_taps'])

    def required_params(self):
        return ['n_taps', 'n_inputs']

    def forward(self, x):
        x = self.lin_1(x)
        return x


# same as NetLinearLayer, but with non-linear activation function
class NetLOneLayer(nn.Module):
    def __init__(self, network_params = {'n_taps': 10, 'n_inputs': 10}):
        super(NetLOneLayer, self).__init__()
        self.lin_1 = nn.Linear(network_params['n_inputs'], network_params['n_taps'])

    def required_params(self):
        return ['n_taps', 'n_inputs']

    def forward(self, x):
        x = self.lin_1(x)
        return x


# input size is not necessarily the same as number of nodes in first layer
class NetTwoLayer(nn.Module):
    def __init__(self, network_params = {'n_taps': [10, 1], 'n_inputs': 10}):
        super(NetTwoLayer, self).__init__()
        self.lin_1 = nn.Linear(network_params['n_inputs'], network_params['n_taps'][0])
        self.lin_2 = nn.Linear(network_params['n_taps'][0], network_params['n_taps'][1])
        self.rl_1 = nn.ReLU()

    def required_params(self):
        return ['n_taps', 'n_inputs']

    def forward(self, x):
        x = self.lin_1(x)
        x = self.rl_1(x)
        x = self.lin_2(x)
        return x


class NetThreeLayer(nn.Module):
    def __init__(self, network_params = {'n_taps': [10, 10, 1], 'n_inputs': 10}):
        super(NetThreeLayer, self).__init__()
        self.lin_1=nn.Linear(network_params['n_inputs'], network_params['n_taps'][0])
        self.lin_2=nn.Linear(network_params['n_taps'][0], network_params['n_taps'][1])
        self.lin_3=nn.Linear(network_params['n_taps'][1], network_params['n_taps'][2])
        self.rl_1=nn.ReLU()
        self.rl_2=nn.ReLU()

    def required_params(self):
        return ['n_taps', 'n_inputs']

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.lin_2(x)
        x=self.rl_2(x)
        x=self.lin_3(x)
        return x


class NetFourLayer(nn.Module):
    def __init__(self, network_params = {'n_taps': [10, 10, 10, 1], 'n_inputs': 10}):
        super(NetFourLayer, self).__init__()
        self.lin_1=nn.Linear(network_params['n_inputs'], network_params['n_taps'][0])
        self.lin_2=nn.Linear(network_params['n_taps'][0], network_params['n_taps'][1])
        self.lin_3=nn.Linear(network_params['n_taps'][1], network_params['n_taps'][2])
        self.lin_4 = nn.Linear(network_params['n_taps'][2], network_params['n_taps'][3])
        self.rl_1=nn.ReLU()
        self.rl_2=nn.ReLU()
        self.rl_3 = nn.ReLU()

    def required_params(self):
        return ['n_taps', 'n_inputs']

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.lin_2(x)
        x = self.rl_2(x)
        x = self.lin_3(x)
        x = self.rl_3(x)
        x = self.lin_4(x)

        return x




# class NetLinearLayer(nn.Module):
#     def __init__(self):
#         super(NetLinearLayer, self).__init__()
#         self.lin_1 = nn.Linear(300, 1)
#
#     def forward(self, x):
#         x = self.lin_1(x)
#         return x

class NetLinearLayer30(nn.Module):
    def __init__(self):
        super(NetLinearLayer30, self).__init__()
        self.lin_1=nn.Linear(30, 1)

    def forward(self, x):
        x=self.lin_1(x)
        return x

class NetLinearLayer100(nn.Module):
    def __init__(self):
        super(NetLinearLayer100, self).__init__()
        self.lin_1=nn.Linear(100, 1)

    def forward(self, x):
        x=self.lin_1(x)
        return x

class NetLinearLayer600(nn.Module):
    def __init__(self):
        super(NetLinearLayer600, self).__init__()
        self.lin_1=nn.Linear(600, 1)

    def forward(self, x):
        x=self.lin_1(x)
        return x

class NetLinearLayer3000(nn.Module):
    def __init__(self):
        super(NetLinearLayer3000, self).__init__()
        self.lin_1=nn.Linear(3000, 1, bias = False)

    def forward(self, x):
        x=self.lin_1(x)
        return x

class NetLinearLayer30000(nn.Module):
    def __init__(self):
        super(NetLinearLayer30000, self).__init__()
        self.lin_1=nn.Linear(30000, 1, bias = False)

    def forward(self, x):
        x=self.lin_1(x)
        return x

class Net_3k_1k(nn.Module):
    def __init__(self):
        super(Net_3k_1k, self).__init__()

        self.lin_1=nn.Linear(3000, 1000)
        self.lin_2=nn.Linear(1000, 1)
        self.rl_1=nn.ReLU()

    def forward(self, x):
        x=self.lin_1(x)
        x=self.rl_1(x)
        x=self.lin_2(x)
        return x


conf={
    'first_random_noise_test':
        {'dataset_train': 'train_bandlimited_rand_noise_10M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetTest
        },

    'linear_layer':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_10k_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetLinearLayer
        },

    'linear_layer_large':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_6_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_6_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.01,
         'momentum': 0.1,
         'network': NetLinearLayer600
        },

    'linear_layer_u_hat':
        {'dataset_train': 'train_bandlimited_rand_noise_u_hat_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_u_hat_100k_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 25,
         'learning_rate': 0.05,
         'momentum': 0.1,
         'network': NetLinearLayer100
        },

    'linear_layer_u_hat_6_bits':
        {'dataset_train': 'train_bandlimited_rand_noise_u_hat_1M_32_6_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_u_hat_1M_32_6_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 5,
         'learning_rate': 0.001,
         'momentum': 0.1,
         'network': NetLinearLayer100
        },

    'first_random_noise_test_large':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_6_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_6_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetTestLarge
        },

    '1_linear_layer_large_input_layer':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 1000,
         'sample_offset': 500,
         'batch_size': 10000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetLinearLayer3000
        },

    '2_linear_layer_large_input_layer':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 10000,
         'sample_offset': 5000,
         'batch_size': 100,
         'n_iter_batch': 10,
         'learning_rate': 0.00001,
         'momentum': 0.00001,
         'network': NetLinearLayer30000
        },

    '3_students_network':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetTest
        },

    '4_two_layered_1k_inputs':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 1000,
         'sample_offset': 500,
         'batch_size': 100,
         'n_iter_batch': 10,
         'learning_rate': 0.0001,
         'momentum': 0.0001,
         'network': Net_3k_1k
        },

    '5_linear_layer_large_input_layer_small_lr':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 1000,
         'sample_offset': 500,
         'batch_size': 10000,
         'n_iter_batch': 10,
         'learning_rate': 0.001,
         'momentum': 0.001,
         'network': NetLinearLayer3000
        },

    '6_linear_layer_large_input_layer_extra_small_lr':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 1000,
         'sample_offset': 500,
         'batch_size': 10000,
         'n_iter_batch': 10,
         'learning_rate': 0.00001,
         'momentum': 0.00001,
         'network': NetLinearLayer3000
        },

    '7_two_layered_1k_inputs_large_lr':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 1000,
         'sample_offset': 500,
         'batch_size': 100,
         'n_iter_batch': 10,
         'learning_rate': 10,
         'momentum': 0.01,
         'network': Net_3k_1k
        },

    '8_linear_layer_300':
        {'dataset_train': 'train_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'dataset_test': 'eval_bandlimited_rand_noise_1M_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 10,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetLinearLayer
        },

    '9_test_multiple_datasets':
        {'dataset_train': ['eval_bandlimited_rand_noise_100k_32_3_2_2.pkl', 'eval_bandlimited_rand_noise_10k_32_3_2_2.pkl'],
         'dataset_test': 'eval_bandlimited_rand_noise_10k_32_3_2_2.pkl',
         'OSR': 32,
         'window_size': 100,
         'sample_offset': 50,
         'batch_size': 1000,
         'n_iter_batch': 2,
         'learning_rate': 0.1,
         'momentum': 0.1,
         'network': NetLinearLayer
         },


}
