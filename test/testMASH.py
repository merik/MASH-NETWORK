import matplotlib
matplotlib.use('agg')
import AnalogToDigital as ADC
import AnalogToDigital.deltaSigma as deltaSigma
import matplotlib.pyplot as plt

import numpy as np

"""
This runs the Sigma Delta Comparision
"""
size = 8092*4
order = 6
OSR = 16
print("OSR = %s" % OSR)


"""
Input signal
"""
# The steering vector is not used in the sigma delta simulation
dummy = np.zeros(order)
std = 1e-1
# input = ADC.system.BandlimitedNoise(std, OSR)
input = ADC.system.Sin(Ts=1., amplitude = 1./np.sqrt(2), frequency=(1./(2 * OSR) / 3.), phase = 0, steeringVector=np.zeros(order))

"""
Simulation Variables
"""
t = np.arange(size)
u = input.scalarFunction(t)

"""
Setup MASH
"""
ds = [deltaSigma.DeltaSigma(OSR, 2, nlev = 4) for x in range(int(order / 2))]
mash = deltaSigma.MASH(OSR, ds)


"""
Test frequency response of decimation filter
"""
f, h = mash.filter.frequencyResponse()
plt.figure()
plt.plot(f / (np.pi * 2.), 20 * np.log10(np.abs(h)))



"""
Simulate Spectrum
"""
mash.simSpectrum()

"""
Generate Data
"""
bits = mash.simulate(input, t)

"""
Apply cancellation logic
"""

u_rec = mash.reconstruction(bits)

"""
Smooth with Decimation filter
"""
u_hat = mash.lowPassFilter(u_rec)
u_hat = mash.lowPassFilter(u_hat)
u_hat = mash.lowPassFilter(u_hat)
u_hat = mash.lowPassFilter(u_hat)
u_hat = mash.lowPassFilter(u_hat)

plt.figure()
plt.title("Input estimations")
plt.plot(t, u, label="u")
# plt.plot(t, u_rec, label="MASH before filter")
plt.plot(t, u_hat, label="MASH after filter")
# plt.xlabel("t")
plt.legend()

"""
Find correlation
"""
corr = np.correlate(u, u_hat, mode='same')
plt.figure()
plt.plot(20 * np.log10(np.abs(corr)))

plt.show()
