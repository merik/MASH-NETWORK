import evalutation
import numpy as np
# import matplotlib.pyplot as plt

configuration = {
    'OSR': 32,
    'device': "MASH_1_1_1"
}

def testGetSinusData():
    mash_orders = [int(x) for x in configuration['device'][4:].split('_') if x != '']
    print(mash_orders)
    sinDataSample, sinDataLabel= evalutation.getSinusData(amplitude = 1./np.sqrt(2.), osr = configuration['OSR'], order_mash_block = mash_orders, n_lev = 2, size = 1000)

def testSpectrum():
    signal = 1. * np.sin(0.0123 * np.linspace(0, 999, 1000) + 32.)
    psd = evalutation.spectrum(signal)

def testCalculateSNR():
    N = 1000000
    data = np.random.randn(N) * 1e-4 + np.sin(2. * np.pi * np.arange(N) / (2 * configuration['OSR']) * 2/3)
    psd = evalutation.spectrum(data)
    # plt.figure()
    # plt.plot(10 * np.log10(psd))
    # plt.plot([0, psd.size/(configuration['OSR']), psd.size/(configuration['OSR']) + 1, psd.size], [0, 0, 100, 100])
    snr, signal, noise = evalutation.calculateSNR(psd, configuration['OSR'], offset=20)
    print(10 * np.log10(snr), signal, noise)
    # plt.show()


if __name__ == "__main__":
    testGetSinusData()
    testSpectrum()
    testCalculateSNR()
