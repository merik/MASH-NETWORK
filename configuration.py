# Training Configuration File
import json

conf_file_path = "configurations.json"


def validateObject(obj):
    return True

def save(obj):
    if validateObject(obj):
        with open(conf_file_path,'w') as f:
            json.dump(obj, f)
        
def load():
    try:
        with open(conf_file_path,'r') as f:
            return json.load(f)
    except:
        return dict()

def init():
    pass
    
if __name__ == "__main__":
    init()